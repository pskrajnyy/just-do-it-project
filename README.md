# Project description
This is example dashboard. Dashboard written using HTML/CSS and JavaScript based on a project in PSD.
# Project objective
The main purpose of this project was to play a little bit with HtML/CSS and JavaScript.
# Test it live
The application has been deployed on [Heroku](https://www.heroku.com/) cloud . You can test it by yourself on this [website](https://pskrajnyy.gitlab.io/just-do-it-project/).
