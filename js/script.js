const app = {      

    initPages: function () {
        const thisApp = this;
        const idFromHash = window.location.hash.replace('#', '');

        thisApp.pages = document.querySelector('.content').children;
        thisApp.nav = document.querySelector('.sidebar-nav');
        thisApp.navLinks = document.querySelectorAll('.sidebar-nav a');

        let pageMatchingHash = thisApp.pages[0].id;

        for (let page of thisApp.pages) {
            if (page.id == idFromHash) {
                pageMatchingHash = page.id;
                break;
            }
        }

        thisApp.activatePage(pageMatchingHash);

        for (let link of thisApp.navLinks) {
            link.addEventListener('click', function (event) {
                event.preventDefault();
                const clickedElement = this;
                const id = clickedElement.getAttribute('href').replace('#article-', '');

                thisApp.activatePage(id);

                window.location.hash = '#' + id;
            });
        }
    },

    activatePage: function (pageId) {
        const thisApp = this;

        for (let page of thisApp.pages) {
            page.classList.toggle('article-active', page.id == pageId);

            if (page.id == 'links' && pageId == 'general') {
                page.classList.add('article-active');
            }
        }

        for (let link of thisApp.navLinks) {
            link.classList.toggle('active', link.getAttribute('href') == '#article-' + pageId);
        }
        document.body.classList = pageId;
    },

    initChart(type) {
        const thisApp = this;
        const ctx = document.getElementById('myChart').getContext('2d');
        const chart = new Chart(ctx, {
            type: type,
            data: {
                labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"],
                datasets: [{
                    label: "Signups",
                    backgroundColor: '#8DBEC8',
                    borderColor: '#8DBEC8',
                    data: [52, 51, 41, 94, 26, 6, 72, 9, 21, 88],
                },
                {
                    label: "FTD",
                    backgroundColor: '#F29E4E',
                    borderColor: '#F29E4E',
                    data: [6, 72, 1, 0, 47, 11, 50, 44, 63, 76],
                },
                {
                    label: "Earned",
                    backgroundColor: '#71B374',
                    borderColor: '#71B374',
                    data: [59, 49, 68, 90, 67, 41, 13, 38, 48, 48],
                    hidden: true,
                }]
            },
            options: {
                legend: {
                    display: false,
                }
            }
        });

        thisApp.chart = chart;
        console.log(thisApp);
    },

    closeNav() {
        document.getElementById('.section-sidebar').style.width = "0";
     },    

    initListeners() {
        const thisApp = this;
        function toggleMenu(visible) {
            document.querySelector('.section-sidebar').style.display = 'block';
            document.querySelector('.section-sidebar').style.zIndex = '3';
        }

        document.querySelector('.hamburger').addEventListener('click', function(e) {
            e.preventDefault();
            toggleMenu()
        });

        document.querySelector('.close-menu-button').addEventListener('click', function() {
            document.querySelector('.section-sidebar').style.display = 'none';
        })

        window.addEventListener('resize', function() {
            if(thisApp.windowWidth > 1000 && this.window.innerWidth < 1000) {
                thisApp.initChart('horizontalBar');
                thisApp.windowWidth = this.window.innerWidth;
            } else if(thisApp.windowWidth < 1000 && this.window.innerWidth > 1000) {
                thisApp.initChart('bar');
                thisApp.windowWidth = this.window.innerWidth;
            }            
        })
    },

    init: function () {
        const thisApp = this;
        thisApp.windowWidth = window.innerWidth;

        if(thisApp.windowWidth < 1000) {
            thisApp.initChart('horizontalBar');
        } else {
            thisApp.initChart('bar');
        };
        
        thisApp.initPages(); 
        thisApp.initListeners();       
    },
};

app.init();