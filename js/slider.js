const rangeSlider = document.getElementById("slider");
const rangeBullet = document.getElementById("rs-bullet");
showSliderValue();
rangeSlider.addEventListener("input", showSliderValue, false);

function showSliderValue() {
    const barWidth = document.getElementsByClassName('bar')[0].offsetWidth;
    rangeBullet.innerHTML = rangeSlider.value + ' hours';
    let bulletPosition = (rangeSlider.value /rangeSlider.max);
    rangeBullet.style.left = (bulletPosition * barWidth) + "px";
}

const slider = $("#slider");
const fill = $(".bar .fill");

function setBar() {
    let value = slider.val() * 0.595238;
    fill.css("width", value + "%");
}

slider.on("input", setBar);
setBar();