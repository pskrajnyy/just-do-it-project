var inputPassword = document.getElementById('input-password');

inputPassword.addEventListener('input', function() {
    var password = inputPassword.value;

    if(password.length < 4 && password.length > 1 ) {
        inputPassword.classList.add('input-invalid');
    } else {
        inputPassword.classList.remove('input-invalid');
    }
});